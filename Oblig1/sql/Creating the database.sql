CREATE TABLE `assignment1`.`books` 
( `id` INT NOT NULL AUTO_INCREMENT ,
`title` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL ,
`author` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL ,
`description` VARCHAR(2000) CHARACTER SET utf8 COLLATE utf8_danish_ci NULL ,
PRIMARY KEY (`id`)) ENGINE = InnoDB; 