<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
     * @throws PDOException
     */
    public function __construct($db = null)  
    {  
        if ($db) 
        {
            $this->db = $db;
        }
        else
        {
            $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) ); 
        }
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        $stmt = $this->db->query('SELECT * FROM book ORDER BY id');
        
        // Go through and show all books
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $description = $row['description'];
        
            if ($row['description'] === null)
            {
                $description = "";
            }
        
            $booklist[] = new Book($row['title'], $row['author'], $description, $row['id']);
        }
        
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @throws PDOException
     */
    public function getBookById($id)
    {
        if (!is_numeric($id) || $id <= 0) {
            return null;
        }
        $book = null;
        $stmt = $this->db->query('SELECT * FROM book WHERE id=' . $id);
        
        // Take the book (but if it's more add them too)
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            // Be sure that the webapplication understand NULL
            $description = $row['description'];
            if ($row['description'] === NULL)
            {
                $description = "";
            }
        
            $book = new Book($row['title'], $row['author'], $description, $row['id']);
        }

        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
     * @return true/false.
     * @throws PDOException
     */
    public function addBook($book)
    {
        // Check that title and author is written in
        if ($book->title === "" || $book->author === "") {
            return false;
        }
        
        // Check if description-field is empty or not and evt. set to NULL
        if ($book->description == "") {
            $description = NULL;
        }
        else {
            $description = $book->description;
        }
        
        // Add book
        $stmt = $this->db->prepare( 'INSERT INTO book (title, author, description)' . ' VALUES(:title, :author, :description)' );
        $stmt->bindValue(':title', $book->title);
        $stmt->bindValue(':author', $book->author);
        $stmt->bindValue(':description', $description);
        $affectedNo = $stmt->execute();
        $book->id = $this->db->lastInsertId();
        return true;
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @return true/false.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        // Check that title and author is written in
        if ($book->title === "" || $book->author === "") {
            return false;
        }
        
        // Check that id is numeric and a valid id (bigger than 0)
        if (!is_numeric($book->id) || $book->id <= 0) {
            return false;
        }
        
        // Check if description-field is empty or not and evt. set to NULL
        if ($book->description == "") {
            $description = NULL;
        }
        else {
            $description = $book->description;
        }
        
        //Update book
        $stmt = $this->db->prepare( 'UPDATE book SET title=?, author=?, description=? WHERE id=?' );
        $stmt->execute(array($book->title, $book->author, $description, $book->id));
        return true;
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        // If book-id is numeric
        if (!is_numeric($id)) {
            return null;
        }
        
        // Delete book
        $stmt = $this->db->prepare("DELETE FROM book WHERE id=:id");
        $stmt->bindValue(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
    }
    
}

?>